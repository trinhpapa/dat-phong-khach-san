﻿using DatPhongKhachSan.Models;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Data {
    public class ApplicationDbContext : DbContext {
        public ApplicationDbContext (DbContextOptions options) : base (options) { }

        public DbSet<NguoiDung> NguoiDungs { get; set; }
        public DbSet<Quyen> Quyens { get; set; }
        public DbSet<DanhGia> DanhGias { get; set; }
        public DbSet<DatPhong> DatPhongs { get; set; }
        public DbSet<HinhAnh> HinhAnhs { get; set; }
        public DbSet<KhachSan> KhachSans { get; set; }
        public DbSet<BaiViet> BaiViets { get; set; }
    }
}