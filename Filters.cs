﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DatPhongKhachSan
{
    public class LoginRequired : ActionFilterAttribute
    {
        private readonly string[] _role;

        public LoginRequired()
        {
        }

        public LoginRequired(string role)
        {
            _role = role.Split(",").Select(item => item.Trim()).ToArray();
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Session.GetInt32(Constants.SessionKey) == null)
            {
                context.Result = new RedirectResult("/login");
            }
            if (_role != null && !_role.Contains(context.HttpContext.Session.GetString(Constants.SessionRole)))
            {
                context.Result = new RedirectResult("/access-denied");
            }
            base.OnActionExecuting(context);
        }
    }
}