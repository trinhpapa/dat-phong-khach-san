#pragma checksum "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c3a7390d25cf0617d38111d7401f05bfbf06986c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_KhachHang_QuanLyTaiKhoan), @"mvc.1.0.view", @"/Views/KhachHang/QuanLyTaiKhoan.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/KhachHang/QuanLyTaiKhoan.cshtml", typeof(AspNetCore.Views_KhachHang_QuanLyTaiKhoan))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\Projects\dat-phong-khach-san\Views\_ViewImports.cshtml"
using DatPhongKhachSan;

#line default
#line hidden
#line 2 "D:\Development\Projects\dat-phong-khach-san\Views\_ViewImports.cshtml"
using DatPhongKhachSan.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c3a7390d25cf0617d38111d7401f05bfbf06986c", @"/Views/KhachHang/QuanLyTaiKhoan.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"40c2169d56f73bfbd726f2127e350312718b3d77", @"/Views/_ViewImports.cshtml")]
    public class Views_KhachHang_QuanLyTaiKhoan : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DatPhongKhachSan.Models.QuanLyTaiKhoanModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
  
ViewData["Title"] = "Quản lý tài khoản";
Layout = "_LayoutMini";
var page = Context.Request.Query["page"];

#line default
#line hidden
            BeginContext(170, 222, true);
            WriteLiteral("<div class=\"body-wrap bg-light\">\r\n    <div class=\"container\">\r\n        <div class=\"row p-2\">\r\n            <a href=\"/khach-hang/quan-ly-tai-khoan?page=profile\">\r\n                <div class=\"col-4\">\r\n                    <div");
            EndContext();
            BeginWriteAttribute("class", " class=\'", 392, "\'", 461, 4);
            WriteAttributeValue("", 400, "shadow", 400, 6, true);
            WriteAttributeValue(" ", 406, "rounded", 407, 8, true);
            WriteAttributeValue(" ", 414, "item-menu", 415, 10, true);
#line 12 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
WriteAttributeValue(" ", 424, page == "profile" ? "active" : "", 425, 36, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(462, 449, true);
            WriteLiteral(@">
                        <div class=""row no-gutters align-items-center"">
                            <div class=""col mr-2"">Thông tin tài khoản</div>
                            <div class=""col-auto""><i class=""fad fa-id-card""></i></div>
                        </div>
                    </div>
            </a>
        </div>
        <a href=""/khach-hang/quan-ly-tai-khoan?page=hotel"">
            <div class=""col-4"">
                <div");
            EndContext();
            BeginWriteAttribute("class", " class=\'", 911, "\'", 978, 4);
            WriteAttributeValue("", 919, "shadow", 919, 6, true);
            WriteAttributeValue(" ", 925, "rounded", 926, 8, true);
            WriteAttributeValue(" ", 933, "item-menu", 934, 10, true);
#line 22 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
WriteAttributeValue(" ", 943, page == "hotel" ? "active" : "", 944, 34, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(979, 406, true);
            WriteLiteral(@">
                    <div class=""row no-gutters align-items-center"">
                        <div class=""col mr-2"">Khách sạn của bạn</div>
                        <div class=""col-auto""><i class=""fad fa-hotel""></i></div>
                    </div>
                </div>
        </a>
    </div>
    <a href=""/khach-hang/quan-ly-tai-khoan?page=suport"">
        <div class=""col-4"">
            <div");
            EndContext();
            BeginWriteAttribute("class", " class=\'", 1385, "\'", 1453, 4);
            WriteAttributeValue("", 1393, "shadow", 1393, 6, true);
            WriteAttributeValue(" ", 1399, "rounded", 1400, 8, true);
            WriteAttributeValue(" ", 1407, "item-menu", 1408, 10, true);
#line 32 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
WriteAttributeValue(" ", 1417, page == "suport" ? "active" : "", 1418, 35, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1454, 291, true);
            WriteLiteral(@">
                <div class=""row no-gutters align-items-center"">
                    <div class=""col mr-2"">Hỗ trợ / khiếu nại</div>
                    <div class=""col-auto""><i class=""fad fa-user-headset""></i></div>
                </div>
            </div>
    </a>
</div>
</div>
");
            EndContext();
#line 41 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
 if(page=="profile"){

#line default
#line hidden
            BeginContext(1769, 50, false);
#line 42 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
Write(Html.Partial("_ThongTinTaiKhoan", Model.NguoiDung));

#line default
#line hidden
            EndContext();
#line 42 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
                                                   
}

#line default
#line hidden
#line 44 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
 if(page=="hotel"){

#line default
#line hidden
            BeginContext(1846, 50, false);
#line 45 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
Write(Html.Partial("_DanhSachKhachSan", Model.KhachSans));

#line default
#line hidden
            EndContext();
#line 45 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
                                                   
}

#line default
#line hidden
#line 47 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
 if(page=="suport"){

#line default
#line hidden
            BeginContext(1924, 30, false);
#line 48 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
Write(Html.Partial("_HoTroKhieuNai"));

#line default
#line hidden
            EndContext();
#line 48 "D:\Development\Projects\dat-phong-khach-san\Views\KhachHang\QuanLyTaiKhoan.cshtml"
                               
}

#line default
#line hidden
            BeginContext(1959, 14, true);
            WriteLiteral("</div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DatPhongKhachSan.Models.QuanLyTaiKhoanModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
