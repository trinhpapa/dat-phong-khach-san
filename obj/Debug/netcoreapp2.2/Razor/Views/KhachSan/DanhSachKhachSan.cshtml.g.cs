#pragma checksum "D:\Development\Projects\dat-phong-khach-san\Views\KhachSan\DanhSachKhachSan.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0d713f5feb0ea8ddf62c27f530a0eadef4468a5a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_KhachSan_DanhSachKhachSan), @"mvc.1.0.view", @"/Views/KhachSan/DanhSachKhachSan.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/KhachSan/DanhSachKhachSan.cshtml", typeof(AspNetCore.Views_KhachSan_DanhSachKhachSan))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Development\Projects\dat-phong-khach-san\Views\_ViewImports.cshtml"
using DatPhongKhachSan;

#line default
#line hidden
#line 2 "D:\Development\Projects\dat-phong-khach-san\Views\_ViewImports.cshtml"
using DatPhongKhachSan.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0d713f5feb0ea8ddf62c27f530a0eadef4468a5a", @"/Views/KhachSan/DanhSachKhachSan.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"40c2169d56f73bfbd726f2127e350312718b3d77", @"/Views/_ViewImports.cshtml")]
    public class Views_KhachSan_DanhSachKhachSan : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\Development\Projects\dat-phong-khach-san\Views\KhachSan\DanhSachKhachSan.cshtml"
  
Layout = "_LayoutMini";

#line default
#line hidden
            BeginContext(32, 10657, true);
            WriteLiteral(@"<div style=""margin-top: 100px"">
	<section class=""ftco-section ftco-no-pt"">
		<div class=""container"">
			<div class=""row justify-content-center pb-4"">
				<div class=""col-md-12 heading-section text-center ftco-animate"">
					<h2 class=""mb-4"">Danh sách khách sạn</h2>
				</div>
			</div>
			<div class=""row"">
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""/khach-san/1"" class=""img"" style=""background-image: url(images/destination-1.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">8 Days Tour</span>
							<h3><a href=""/khach-san/1"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-mountains""></span>Near Mountain</li>
							</ul>
						</div>
					</div>
				</div>
				<div ");
            WriteLiteral(@"class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-2.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">10 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-3.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""");
            WriteLiteral(@"location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>

				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-4.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">8 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 f");
            WriteLiteral(@"tco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-5.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">10 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span c");
            WriteLiteral(@"lass=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
				");
            WriteLiteral(@"	<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""><");
            WriteLiteral(@"/span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project");
            WriteLiteral(@"-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indones");
            WriteLiteral(@"ia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a hr");
            WriteLiteral(@"ef=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""col-md-4 ftco-animate"">
					<div class=""project-wrap"">
						<a href=""#"" class=""img"" style=""background-image: url(images/destination-6.jpg);""></a>
						<div class=""text p-4"">
							<span class=""price"">$300/person</span>
							<span class=""days"">7 Days Tour</span>
							<h3><a href=""#"">Bali, Indonesia</a></h3>
							<p class=""location""><span class=""ion-ios-map""></span> Bali, Indonesia</p>
							<ul>");
            WriteLiteral(@"
								<li><span class=""flaticon-shower""></span>2</li>
								<li><span class=""flaticon-king-size""></span>3</li>
								<li><span class=""flaticon-sun-umbrella""></span>Near Beach</li>
							</ul>
						</div>
					</div>
				</div>
				<div class=""text-center w-100 ftco-animate"">
					<p><a href=""#"" class=""btn btn-primary py-3 px-4"">Xem thêm</a></p>
				</div>
			</div>
		</div>
	</section>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
