﻿namespace DatPhongKhachSan
{
    public class Constants
    {
        public static string SessionKey = "nguoidung-id";
        public static string SessionUsername = "nguoidung-tendangnhap";
        public static string SessionRole = "nguoidung-quyen";
        public static string SessionDisplayName = "nguoidung-tenhienthi";
    }
}