﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatPhongKhachSan.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Quyens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ten = table.Column<string>(maxLength: 256, nullable: true),
                    NgayTao = table.Column<DateTime>(nullable: false),
                    TrangThai = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quyens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NguoiDungs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TenDangNhap = table.Column<string>(maxLength: 256, nullable: false),
                    Ten = table.Column<string>(maxLength: 256, nullable: true),
                    Ho = table.Column<string>(maxLength: 256, nullable: true),
                    MatKhauMaHoa = table.Column<string>(maxLength: 256, nullable: true),
                    MatKhauKhoa = table.Column<string>(maxLength: 10, nullable: true),
                    QuyenId = table.Column<int>(nullable: false),
                    NgayTao = table.Column<DateTime>(nullable: false),
                    TrangThai = table.Column<bool>(nullable: false),
                    NgayThamGia = table.Column<DateTime>(nullable: false),
                    SoCMND = table.Column<string>(nullable: true),
                    DiaChi = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SoDienThoai = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NguoiDungs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NguoiDungs_Quyens_QuyenId",
                        column: x => x.QuyenId,
                        principalTable: "Quyens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BaiViets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TieuDe = table.Column<string>(nullable: true),
                    NoiDung = table.Column<string>(nullable: true),
                    ThoiGian = table.Column<DateTime>(nullable: false),
                    NguoiDangId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaiViets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaiViets_NguoiDungs_NguoiDangId",
                        column: x => x.NguoiDangId,
                        principalTable: "NguoiDungs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KhachSans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TenKhachSan = table.Column<string>(nullable: true),
                    Sao = table.Column<short>(nullable: false),
                    SoDienThoai = table.Column<string>(nullable: true),
                    GiaThue = table.Column<double>(nullable: false),
                    SoPhong = table.Column<short>(nullable: false),
                    TinhThanh = table.Column<string>(nullable: true),
                    QuanHuyen = table.Column<string>(nullable: true),
                    PhuongXa = table.Column<string>(nullable: true),
                    DiaChi = table.Column<string>(nullable: true),
                    BaiViet = table.Column<string>(nullable: true),
                    ThongTinKhac = table.Column<string>(nullable: true),
                    FreeWifi = table.Column<bool>(nullable: false),
                    MayLanh = table.Column<bool>(nullable: false),
                    TruyenHinhCap = table.Column<bool>(nullable: false),
                    NuocKhoang = table.Column<bool>(nullable: false),
                    Giuong = table.Column<bool>(nullable: false),
                    AnSang = table.Column<bool>(nullable: false),
                    NguoiTao = table.Column<int>(nullable: true),
                    ThoiGianTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhachSans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KhachSans_NguoiDungs_NguoiTao",
                        column: x => x.NguoiTao,
                        principalTable: "NguoiDungs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DanhGias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NguoiDanhGiaId = table.Column<int>(nullable: true),
                    KhachSanId = table.Column<int>(nullable: false),
                    NhanXet = table.Column<string>(nullable: true),
                    DanhGiaSao = table.Column<short>(nullable: false),
                    AnDanh = table.Column<bool>(nullable: false),
                    ThoiGianDanhGia = table.Column<DateTime>(nullable: false),
                    DaDuocDuyet = table.Column<bool>(nullable: false),
                    NguoiDuyetId = table.Column<int>(nullable: true),
                    ThoiGianDuyet = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DanhGias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DanhGias_KhachSans_KhachSanId",
                        column: x => x.KhachSanId,
                        principalTable: "KhachSans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DanhGias_NguoiDungs_NguoiDanhGiaId",
                        column: x => x.NguoiDanhGiaId,
                        principalTable: "NguoiDungs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DanhGias_NguoiDungs_NguoiDuyetId",
                        column: x => x.NguoiDuyetId,
                        principalTable: "NguoiDungs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DatPhongs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NguoiThueId = table.Column<int>(nullable: false),
                    PhongKhachSanId = table.Column<int>(nullable: false),
                    ThoiGianThue = table.Column<DateTime>(nullable: false),
                    ThoiGianBatDau = table.Column<DateTime>(nullable: false),
                    ThoiGianKetThuc = table.Column<DateTime>(nullable: false),
                    TienPhong = table.Column<double>(nullable: false),
                    TienDichVu = table.Column<double>(nullable: true),
                    TongTienTamTinh = table.Column<double>(nullable: true),
                    HinhThucThanhToan = table.Column<short>(nullable: false),
                    TrangThaiThanhToan = table.Column<short>(nullable: false),
                    GhiChu = table.Column<string>(nullable: true),
                    KhachSanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatPhongs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatPhongs_KhachSans_KhachSanId",
                        column: x => x.KhachSanId,
                        principalTable: "KhachSans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DatPhongs_NguoiDungs_NguoiThueId",
                        column: x => x.NguoiThueId,
                        principalTable: "NguoiDungs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HinhAnhs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KhachSanId = table.Column<int>(nullable: false),
                    PhongKhachSanId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    TieuDe = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HinhAnhs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HinhAnhs_KhachSans_KhachSanId",
                        column: x => x.KhachSanId,
                        principalTable: "KhachSans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaiViets_NguoiDangId",
                table: "BaiViets",
                column: "NguoiDangId");

            migrationBuilder.CreateIndex(
                name: "IX_DanhGias_KhachSanId",
                table: "DanhGias",
                column: "KhachSanId");

            migrationBuilder.CreateIndex(
                name: "IX_DanhGias_NguoiDanhGiaId",
                table: "DanhGias",
                column: "NguoiDanhGiaId");

            migrationBuilder.CreateIndex(
                name: "IX_DanhGias_NguoiDuyetId",
                table: "DanhGias",
                column: "NguoiDuyetId");

            migrationBuilder.CreateIndex(
                name: "IX_DatPhongs_KhachSanId",
                table: "DatPhongs",
                column: "KhachSanId");

            migrationBuilder.CreateIndex(
                name: "IX_DatPhongs_NguoiThueId",
                table: "DatPhongs",
                column: "NguoiThueId");

            migrationBuilder.CreateIndex(
                name: "IX_HinhAnhs_KhachSanId",
                table: "HinhAnhs",
                column: "KhachSanId");

            migrationBuilder.CreateIndex(
                name: "IX_KhachSans_NguoiTao",
                table: "KhachSans",
                column: "NguoiTao");

            migrationBuilder.CreateIndex(
                name: "IX_NguoiDungs_QuyenId",
                table: "NguoiDungs",
                column: "QuyenId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaiViets");

            migrationBuilder.DropTable(
                name: "DanhGias");

            migrationBuilder.DropTable(
                name: "DatPhongs");

            migrationBuilder.DropTable(
                name: "HinhAnhs");

            migrationBuilder.DropTable(
                name: "KhachSans");

            migrationBuilder.DropTable(
                name: "NguoiDungs");

            migrationBuilder.DropTable(
                name: "Quyens");
        }
    }
}
