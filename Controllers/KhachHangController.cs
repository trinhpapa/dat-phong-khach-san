using System.Linq;
using DatPhongKhachSan.Helpers;
using DatPhongKhachSan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Controllers
{
    public class KhachHangController : BaseController
    {

        [HttpGet("khach-hang/dang-ky")]
        public IActionResult DangKyTaiKhoan()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DangKyTaiKhoan(DangKyModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["Error"] = "Dữ liệu không chính xác";
                return View("DangKyTaiKhoan");
            }

            var kiemTraTenDangNhap = Context.NguoiDungs.Any(item => item.TenDangNhap == model.TenDangNhap);
            if (kiemTraTenDangNhap)
            {
                TempData["Error"] = "Tên đăng nhập đã được sử dụng";
                return View("DangKyTaiKhoan");
            }

            var matKhauKhoa = PasswordEncryption.GeneratePasswordKey();

            var nguoiDung = new NguoiDung()
            {
                QuyenId = Context.Quyens.FirstOrDefault(q => q.Ten == "Khách hàng").Id,
                TenDangNhap = model.TenDangNhap,
                MatKhauKhoa = matKhauKhoa,
                MatKhauMaHoa = PasswordEncryption.EncryptionPasswordWithKey(model.MatKhau, matKhauKhoa)
            };

            Context.NguoiDungs.Add(nguoiDung);
            Context.SaveChanges();
            TempData["Result"] = "Đăng ký thành công";
            return View("DangKyTaiKhoan");
        }

        public IActionResult DanhSachKhachHang()
        {
            var data = Context.NguoiDungs.ToList();
            return View("_DanhSachKhachHang", data);
        }

        [HttpGet("khach-hang/quan-ly-tai-khoan")]
        public IActionResult QuanLyTaiKhoan()
        {
            var nguoiDungId = HttpContext.Session.GetInt32(Constants.SessionKey);
            var model = new QuanLyTaiKhoanModel();
            model.NguoiDung = Context.NguoiDungs.Find(nguoiDungId);
            model.KhachSans = Context.KhachSans.Where(item => item.NguoiTao == nguoiDungId).Include(item => item.HinhAnhs).ToList();
            return View(model);
        }
    }
}