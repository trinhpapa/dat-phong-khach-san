using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using DatPhongKhachSan.Helpers;
using DatPhongKhachSan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Controllers {
    public class KhachSanController : BaseController {
        private IHostingEnvironment _hostingEnvironment;
        public KhachSanController (IHostingEnvironment hostingEnvironment) {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index () {
            return View ();
        }

        [HttpGet ("khach-san/{id}")]
        public IActionResult ChiTietKhachSan (int id) {
            var khachSan = Context.KhachSans.Find (id);
            return View (khachSan);
        }

        [HttpGet ("khach-san")]
        public IActionResult DanhSachKhachSan () {
            return View ();
        }

        [HttpGet ("khach-san/tao-khach-san")]
        public IActionResult TaoKhachSan () {
            return View ("TaoKhachSan");
        }

        [HttpPost]
        [LoginRequired]
        public IActionResult TaoKhachSan (KhachSan model, List<IFormFile> slides) {
            if (slides.Count < 1) {
                TempData["Error"] = "Ảnh không được để trống";
                return View ("TaoKhachSan", model);
            }
            var webRoot = _hostingEnvironment.WebRootPath;
            var contentPath = _hostingEnvironment.ContentRootPath;
            using (var trans = Context.Database.BeginTransaction ()) {
                try {
                    model.ThoiGianTao = DateTime.Now;
                    model.NguoiTao = (int) HttpContext.Session.GetInt32 (Constants.SessionKey);
                    Context.KhachSans.Add (model);

                    foreach (var image in slides) {
                        var fileName = ContentDispositionHeaderValue.Parse (image.ContentDisposition).FileName.Trim ('"');

                        var myUniqueFileName = Convert.ToString (Guid.NewGuid ());

                        var FileExtension = Path.GetExtension (fileName);

                        var newFileName = myUniqueFileName + FileExtension;

                        var path = Path.Combine (_hostingEnvironment.WebRootPath, "Upload/Images");

                        fileName = path + $@"\{newFileName}";

                        if (!Directory.Exists (path)) {
                            Directory.CreateDirectory (path);
                        }

                        using (var fs = System.IO.File.Create (fileName)) {
                            image.CopyTo (fs);
                            fs.Flush ();
                            var hinhAnh = new HinhAnh () {
                                KhachSanId = model.Id,
                                Url = "/Upload/Images" + $@"/{newFileName}",
                                Type = HinhAnhType.Slide
                            };
                            Context.HinhAnhs.Add (hinhAnh);
                        }
                    }
                    Context.SaveChanges ();
                    trans.Commit ();
                    TempData["Result"] = "Khách sạn của bạn đã được thêm thành công, vui lòng chờ đến khi thông tin của bạn được xác thực";
                } catch (Exception exc) {
                    TempData["Error"] = exc;
                    trans.Rollback ();
                }
            }

            return View ("TaoKhachSan", model);
        }
    }
}