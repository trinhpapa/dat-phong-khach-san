using System;
using System.Linq;
using DatPhongKhachSan.Helpers;
using DatPhongKhachSan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Controllers {
    public class AdminController : BaseController {
        public IActionResult KhoiTaoDuLieu () {

            if (!Context.Quyens.Any (item => item.Ten == "Administrator")) {
                var quyen = new Quyen {
                Ten = "Administrator",
                NgayTao = DateTime.Now,
                TrangThai = true
                };
                Context.Quyens.Add (quyen);
            }
            if (!Context.Quyens.Any (item => item.Ten == "Khách hàng")) {
                var quyen = new Quyen {
                Ten = "Khách hàng",
                NgayTao = DateTime.Now,
                TrangThai = true
                };
                Context.Quyens.Add (quyen);
            }
            if (!Context.Quyens.Any (item => item.Ten == "Nhân viên")) {
                var quyen = new Quyen {
                Ten = "Khách hàng",
                NgayTao = DateTime.Now,
                TrangThai = true
                };
                Context.Quyens.Add (quyen);
            }
            if (!Context.NguoiDungs.Any (item => item.TenDangNhap == "Admin")) {
                var matKhauKhoa = PasswordEncryption.GeneratePasswordKey ();
                var nguoiDung = new NguoiDung {
                    TenDangNhap = "Admin",
                    Ho = "Quản trị",
                    Ten = "Hệ thống",
                    MatKhauKhoa = matKhauKhoa,
                    MatKhauMaHoa = PasswordEncryption.EncryptionPasswordWithKey ("admin", matKhauKhoa),
                    QuyenId = 1,
                    NgayTao = DateTime.Now,
                };
            }

            Context.SaveChanges ();

            return Content ("Ok");
        }
    }
}