﻿using System.Linq;
using DatPhongKhachSan.Helpers;
using DatPhongKhachSan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Controllers {
    public class AccountController : BaseController {
        [LoginRequired]
        public IActionResult Index () {
            var userId = HttpContext.Session.GetInt32 (Constants.SessionKey);
            var user = Context.NguoiDungs.Find (userId);

            var model = new TaiKhoanModel {
                NguoiDung = user,
                DoiMatKhau = new DoiMatKhauModel ()
            };

            return View (model);
        }

        public IActionResult DangNhap () {
            return View ();
        }

        [HttpPost ("tai-khoan/dang-nhap")]
        public IActionResult DangNhap ([FromForm] DangNhapModel model) {
            if (!ModelState.IsValid) return Redirect ("/khach-hang/dang-ky");
            var nguoiDung = Context.NguoiDungs.AsNoTracking ().FirstOrDefault (u => u.TenDangNhap == model.TenDangNhap);
            if (nguoiDung == null) {
                TempData["Error"] = "Tài khoản hoặc mật khẩu không đúng";
                return Redirect ("/khach-hang/dang-ky");
            }

            if (!nguoiDung.TrangThai) {
                TempData["Error"] = "Tài khoản đã bị khóa";
                return Redirect ("/khach-hang/dang-ky");
            }

            var matKhauMaHoa = PasswordEncryption.EncryptionPasswordWithKey (model.MatKhau, nguoiDung.MatKhauKhoa);
            if (nguoiDung.MatKhauMaHoa != matKhauMaHoa) {
                TempData["Error"] = "Tài khoản hoặc mật khẩu không đúng";
                return Redirect ("/khach-hang/dang-ky");
            }

            var quyenNguoiDung = Context.Quyens.Find (nguoiDung.QuyenId);
            HttpContext.Session.SetInt32 (Constants.SessionKey, nguoiDung.Id);
            HttpContext.Session.SetString (Constants.SessionUsername, nguoiDung.TenDangNhap);
            HttpContext.Session.SetString (Constants.SessionDisplayName, nguoiDung.Ho + " " + nguoiDung.Ten);
            HttpContext.Session.SetString (Constants.SessionRole, quyenNguoiDung.Ten);
            return Redirect ("/");
        }

        [LoginRequired]
        public IActionResult DangXuat () {
            HttpContext.Session.Clear ();
            return Redirect ("/DangNhap");
        }

        public IActionResult QuenMatKhau () {
            return View ();
        }

        [LoginRequired]
        [HttpPost ("tai-khoan/doi-thong-tin")]
        public IActionResult DoiThongTin (NguoiDung model) {
            var maNguoiDung = HttpContext.Session.GetInt32 (Constants.SessionKey);
            var nguoiDung = Context.NguoiDungs.Find (maNguoiDung);
            nguoiDung.Ten = model.Ten;
            nguoiDung.Ho = model.Ho;
            nguoiDung.SoCMND = model.SoCMND;
            nguoiDung.SoDienThoai = model.SoDienThoai;
            nguoiDung.Email = model.Email;
            nguoiDung.DiaChi = model.DiaChi;
            Context.SaveChanges ();
            TempData["Result"] = "Cập nhật thành công";

            return RedirectToAction ("QuanLyTaiKhoan", "KhachHang", new { page = "profile" });
        }

        [LoginRequired]
        public IActionResult DoiMatKhau (DoiMatKhauModel model) {
            var maNguoiDung = HttpContext.Session.GetInt32 (Constants.SessionKey);
            var nguoiDung = Context.NguoiDungs.Find (maNguoiDung);

            if (model.NhapLaiMatKhau != model.MatKhau) {
                TempData["Error"] = "Mật khẩu mới không trùng khớp";
                var resultError = new TaiKhoanModel {
                    NguoiDung = nguoiDung,
                    DoiMatKhau = model
                };
                return View ("Index", resultError);
            }

            var matKhauCu = PasswordEncryption.EncryptionPasswordWithKey (model.MatKhauCu, nguoiDung.MatKhauKhoa);

            if (matKhauCu != nguoiDung.MatKhauMaHoa) {
                TempData["Error"] = "Mật khẩu cũ không đúng";
                var resultError = new TaiKhoanModel {
                    NguoiDung = nguoiDung,
                    DoiMatKhau = model
                };
                return View ("Index", resultError);
            }

            var matKhauKhoa = PasswordEncryption.GeneratePasswordKey ();

            nguoiDung.MatKhauMaHoa = PasswordEncryption.EncryptionPasswordWithKey (model.MatKhau, matKhauKhoa);
            nguoiDung.MatKhauKhoa = matKhauKhoa;

            Context.SaveChanges ();

            TempData["Result"] = "Cập nhật thành công";

            var result = new TaiKhoanModel {
                NguoiDung = nguoiDung,
                DoiMatKhau = new DoiMatKhauModel ()
            };

            return View ("Index", result);
        }
    }
}