using System.Linq;
using DatPhongKhachSan.Helpers;
using DatPhongKhachSan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatPhongKhachSan.Controllers
{
    public class TrangChuController : BaseController
    {
        public IActionResult Index()
        {
            var model = new TrangChuModel();
            model.KhachSans = Context.KhachSans.Take(6).ToList();
            return View(model);
        }

        [HttpGet("thong-tin")]
        public IActionResult ThongTin()
        {
            return View();
        }
    }
}