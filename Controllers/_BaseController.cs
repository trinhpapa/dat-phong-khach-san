﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DatPhongKhachSan.Data;

namespace DatPhongKhachSan.Controllers
{
    public class BaseController : Controller
    {
        protected ApplicationDbContext Context => (ApplicationDbContext)HttpContext.RequestServices.GetService(typeof(ApplicationDbContext));
    }
}