﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DatPhongKhachSan.Helpers
{
    public class PasswordEncryption
    {
        private static string Md5Hash(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            using (var md5Hash = MD5.Create())
            {
                var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sBuilder = new StringBuilder();
                foreach (var t in data)
                {
                    sBuilder.Append(t.ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }

        public static string EncryptionPasswordWithKey(string password, string key)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(key))
            {
                return null;
            }

            return Md5Hash(Md5Hash(password) + key);
        }

        public static string GeneratePasswordKey()
        {
            var rd = new Random();
            const string chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 10)
               .Select(s => s[rd.Next(s.Length)]).ToArray());
        }
    }
}