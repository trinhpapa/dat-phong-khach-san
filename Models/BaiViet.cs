using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class BaiViet {
        public int Id { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public DateTime ThoiGian { get; set; }
        public int NguoiDangId { get; set; }

        [ForeignKey ("NguoiDangId")]
        public virtual NguoiDung NguoiDang { get; set; }
    }
}