﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class DanhGia {
        public int Id { get; set; }
        public int? NguoiDanhGiaId { get; set; }
        public int KhachSanId { get; set; }
        public string NhanXet { get; set; }
        public short DanhGiaSao { get; set; }
        public bool AnDanh { get; set; }
        public DateTime ThoiGianDanhGia { get; set; }
        public bool DaDuocDuyet { get; set; }
        public int? NguoiDuyetId { get; set; }
        public DateTime ThoiGianDuyet { get; set; }

        [ForeignKey ("KhachSanId")]
        public virtual KhachSan KhachSan { get; set; }

        [ForeignKey ("NguoiDanhGiaId")]
        public virtual NguoiDung NguoiDanhGia { get; set; }

        [ForeignKey ("NguoiDuyetId")]
        public virtual NguoiDung NguoiDuyet { get; set; }

    }
}