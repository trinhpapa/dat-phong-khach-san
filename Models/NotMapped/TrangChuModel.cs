using System.Collections.Generic;

namespace DatPhongKhachSan.Models {
    public class TrangChuModel {
        public int Id { get; set; }
        public List<KhachSan> KhachSans { get; set; }
    }
}