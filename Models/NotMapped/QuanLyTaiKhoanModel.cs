﻿using System.Collections.Generic;

namespace DatPhongKhachSan.Models
{
    public class QuanLyTaiKhoanModel
    {
        public List<KhachSan> KhachSans { get; set; }
        public NguoiDung NguoiDung { get; set; }
    }
}