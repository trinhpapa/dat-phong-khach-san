﻿using System.ComponentModel.DataAnnotations;

namespace DatPhongKhachSan.Models
{
    public class TaiKhoanModel
    {
        public NguoiDung NguoiDung { get; set; }

        public DoiMatKhauModel DoiMatKhau { get; set; }
    }

    public class DoiMatKhauModel
    {
        [Display(Name = "Mật khẩu cũ")]
        public string MatKhauCu { get; set; }

        [Display(Name = "Mật khẩu mới")]
        public string MatKhau { get; set; }

        [Display(Name = "Nhập lại mật khẩu mới")]
        public string NhapLaiMatKhau { get; set; }
    }
}