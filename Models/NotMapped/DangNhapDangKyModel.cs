﻿using System.ComponentModel.DataAnnotations;

namespace DatPhongKhachSan.Models
{

    public class DangNhapDangKyModel
    {
        public DangKyModel DangKyModel { get; set; }
        public DangNhapModel DangNhapModel { get; set; }
    }

    public class DangKyModel
    {
        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
        [MinLength(2, ErrorMessage = "Tên đăng nhập tối đa 2 ký tự")]
        [Required(ErrorMessage = "Tên đăng nhập không được để trống")]
        [Display(Name = "Tên đăng nhập")]
        public string TenDangNhap { get; set; }

        [Required(ErrorMessage = "Tên đăng nhập không được để trống")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
        [MinLength(4, ErrorMessage = "Mật khẩu tối thiểu 4 ký tự")]
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [Display(Name = "Mật khẩu")]
        public string MatKhau { get; set; }
    }
    public class DangNhapModel
    {
        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
        [MinLength(2, ErrorMessage = "Tên đăng nhập tối đa 2 ký tự")]
        [Required(ErrorMessage = "Tên đăng nhập không được để trống")]
        [Display(Name = "Tên đăng nhập")]
        public string TenDangNhap { get; set; }

        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
        [MinLength(4, ErrorMessage = "Mật khẩu tối thiểu 4 ký tự")]
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [Display(Name = "Mật khẩu")]
        public string MatKhau { get; set; }
    }
}