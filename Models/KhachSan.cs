﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class KhachSan {
        public int Id { get; set; }
        public string TenKhachSan { get; set; }
        public short Sao { get; set; }
        public string SoDienThoai { get; set; }
        public double GiaThue { get; set; }
        public short SoPhong { get; set; }
        public string TinhThanh { get; set; }
        public string QuanHuyen { get; set; }
        public string PhuongXa { get; set; }
        public string DiaChi { get; set; }
        public string BaiViet { get; set; }
        public string ThongTinKhac { get; set; }

        public bool FreeWifi { get; set; }
        public bool MayLanh { get; set; }
        public bool TruyenHinhCap { get; set; }
        public bool NuocKhoang { get; set; }
        public bool Giuong { get; set; }
        public bool AnSang { get; set; }
        public int? NguoiTao { get; set; }
        public DateTime ThoiGianTao { get; set; }

        [NotMapped]
        public int LuotThue { get; set; }

        [NotMapped]
        public double SoTienThanhToan { get; set; }

        [ForeignKey ("NguoiTao")]
        public virtual NguoiDung NguoiDung { get; set; }

        public virtual ICollection<HinhAnh> HinhAnhs { get; set; }
        public virtual ICollection<DanhGia> DanhGias { get; set; }
    }
}