﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class HinhAnh {
        public int Id { get; set; }
        public int KhachSanId { get; set; }
        public int PhongKhachSanId { get; set; }
        public string Url { get; set; }
        public string TieuDe { get; set; }
        public HinhAnhType Type { get; set; }

        [ForeignKey ("KhachSanId")]
        public KhachSan KhachSan { get; set; }
    }

    public enum HinhAnhType {
        Primary,
        Slide
    }
}