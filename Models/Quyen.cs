﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class Quyen {
        public int Id { get; set; }

        [StringLength (256)]
        public string Ten { get; set; }

        public DateTime NgayTao { get; set; }

        [DefaultValue (true)]
        public bool TrangThai { get; set; } = true;

        [ForeignKey ("QuyenId")]
        public virtual ICollection<NguoiDung> NguoiDungs { get; set; }
    }
}