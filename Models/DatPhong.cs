﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models {
    public class DatPhong {
        public int Id { get; set; }
        public int NguoiThueId { get; set; }
        public int PhongKhachSanId { get; set; }
        public DateTime ThoiGianThue { get; set; }
        public DateTime ThoiGianBatDau { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public double TienPhong { get; set; }
        public double? TienDichVu { get; set; }
        public double? TongTienTamTinh { get; set; }
        public short HinhThucThanhToan { get; set; }
        public short TrangThaiThanhToan { get; set; }
        public string GhiChu { get; set; }

        [ForeignKey ("KhachSanId")]
        public virtual KhachSan KhachSan { get; set; }

        [ForeignKey ("NguoiThueId")]
        public virtual NguoiDung NguoiThue { get; set; }
    }
}