﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatPhongKhachSan.Models
{
    public class NguoiDung
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Tên đăng nhập bắt buộc")]
        [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
        [MinLength(2, ErrorMessage = "Tối thiểu 2 ký tự")]
        [Display(Name = "Tên đăng nhập")]
        public string TenDangNhap { get; set; }

        [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
        [Display(Name = "Tên")]
        public string Ten { get; set; }

        [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
        [Display(Name = "Họ")]
        public string Ho { get; set; }

        [NotMapped]
        [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
        [MinLength(4, ErrorMessage = "Mật khẩu tối thiểu 2 ký tự")]
        [Display(Name = "Mật khẩu")]
        public string MatKhau { get; set; }

        [StringLength(256)]
        public string MatKhauMaHoa { get; set; }

        [StringLength(10)]
        public string MatKhauKhoa { get; set; }

        [Display(Name = "Quyền hạn")]
        public int QuyenId { get; set; }

        public DateTime NgayTao { get; set; }

        [DefaultValue(true)]
        [Display(Name = "Trạng thái")]
        public bool TrangThai { get; set; } = true;

        public virtual Quyen Quyen { get; set; }

        //Dành cho khách hàng
        public DateTime NgayThamGia { get; set; }
        public string SoCMND { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public string SoDienThoai { get; set; }
    }
}